package com.galfins.gnss_compare.DataViewers;

import android.hardware.SensorEvent;
import android.hardware.SensorManager;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.galfins.gnss_compare.CalculationModule;
import com.galfins.gnss_compare.CalculationModulesArrayList;
import com.galfins.gnss_compare.R;
import com.galfins.gogpsextracts.Constellations.SatelliteParameters;
import com.google.common.collect.Sets;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class DataListFragment extends Fragment implements DataViewer{
    private ListView list;
    private TextView title;

    List<String> strList = new ArrayList<>();
    boolean initialized = false;
    ArrayAdapter<String> adapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ViewGroup rootView = (ViewGroup) inflater.inflate(
                R.layout.fragment_log, container, false);

        list = (ListView) rootView.findViewById(R.id.list);
        title = (TextView) rootView.findViewById(R.id.textView);

        adapter = new ArrayAdapter<String>(getActivity(),android.R.layout.simple_list_item_1, strList);

        list.setAdapter(adapter);

        initialized = true;

        title.setText("Satellite Pseudorange");
        return rootView;
    }

    @Override
    public void update(CalculationModulesArrayList calculationModules) {

    }

    @Override
    public void updateOnUiThread(CalculationModulesArrayList calculationModules) {
        if(initialized) {
            List<SatelliteParameters> satellites = new ArrayList<>();
            for(CalculationModule calculationModule : calculationModules){
                for (int i=0; i<calculationModule.getConstellation().getUsedConstellationSize(); i++){

                    // not adding satellites already registered (this is a cross check if a
                    // satellite can be selected from multiple constellations (e.g. a Galileo satellite
                    // will be present in both Galileo and GPS+Galileo constellations.
                    // todo: this has an issue when using e.g. WLS which can result in
                    // todo: erroneous position, causing SUPL to fail - satellite is then
                    // todo: not displayed
                    boolean satelliteFound = false;

                    for(SatelliteParameters sat : satellites){

                        if(sat.getUniqueSatId() != null) {
                            if (sat.getUniqueSatId().equals(calculationModule.getConstellation().getSatellite(i).getUniqueSatId())) {
                                satelliteFound = true;
                                break;
                            }
                        }
                    }
                    if(!satelliteFound)
                        satellites.add(calculationModule.getConstellation().getSatellite(i));
                }
            }
            strList.clear();
            for (SatelliteParameters sat : satellites)
            {
                StringBuilder sb;
                sb = new StringBuilder();
                sb.append("Prn = ");
                sb.append(sat.getUniqueSatId());
                sb.append(" Pseudorange = ");
                DecimalFormat fnum = new DecimalFormat("##0.000");
                sb.append(fnum.format(sat.getPseudorange()));
                strList.add(sb.toString());
            }

            adapter.notifyDataSetChanged();
        }
    }

    @Override
    public void updateSensor(SensorEvent event, SensorManager mSensorManager) {

    }

    @Override
    public void setUnavairable() {

    }
}
