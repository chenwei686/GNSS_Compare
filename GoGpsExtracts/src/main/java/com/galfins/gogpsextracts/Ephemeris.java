//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.galfins.gogpsextracts;

import com.google.protobuf.AbstractParser;
import com.google.protobuf.ByteString;
import com.google.protobuf.CodedInputStream;
import com.google.protobuf.CodedOutputStream;
import com.google.protobuf.ExtensionRegistry;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageV3;
import com.google.protobuf.Internal;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.Message;
import com.google.protobuf.MessageOrBuilder;
import com.google.protobuf.Parser;
import com.google.protobuf.SingleFieldBuilderV3;
import com.google.protobuf.UnknownFieldSet;
import com.google.protobuf.Descriptors.Descriptor;
import com.google.protobuf.Descriptors.FieldDescriptor;
import com.google.protobuf.Descriptors.FileDescriptor;
import com.google.protobuf.Descriptors.OneofDescriptor;
import com.google.protobuf.Descriptors.FileDescriptor.InternalDescriptorAssigner;
import com.google.protobuf.GeneratedMessageV3.FieldAccessorTable;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public final class Ephemeris {
    private static final Descriptor internal_static_gnss_GpsTimeProto_descriptor;
    private static final FieldAccessorTable internal_static_gnss_GpsTimeProto_fieldAccessorTable;
    private static final Descriptor internal_static_gnss_IonosphericModelProto_descriptor;
    private static final FieldAccessorTable internal_static_gnss_IonosphericModelProto_fieldAccessorTable;
    private static FileDescriptor descriptor;

    private Ephemeris() {
    }

    public static void registerAllExtensions(ExtensionRegistryLite registry) {
    }

    public static void registerAllExtensions(ExtensionRegistry registry) {
        registerAllExtensions((ExtensionRegistryLite)registry);
    }

    public static FileDescriptor getDescriptor() {
        return descriptor;
    }

    static {
        String[] descriptorData = new String[]{"\n\u000fephemeris.proto\u0012\u0004gnss\"\"\n\fGpsTimeProto\u0012\u0012\n\nnanosecond\u0018\u0001 \u0001(\u0003\"[\n\u0015IonosphericModelProto\u0012%\n\tdata_time\u0018\u0001 \u0001(\u000b2\u0012.gnss.GpsTimeProto\u0012\r\n\u0005alpha\u0018\u0002 \u0003(\u0001\u0012\f\n\u0004beta\u0018\u0003 \u0003(\u0001B(\n#com.google.location.suplclient.supl\u0080\u0001\u0000"};
        InternalDescriptorAssigner assigner = new InternalDescriptorAssigner() {
            public ExtensionRegistry assignDescriptors(FileDescriptor root) {
                Ephemeris.descriptor = root;
                return null;
            }
        };
        FileDescriptor.internalBuildGeneratedFileFrom(descriptorData, new FileDescriptor[0], assigner);
        internal_static_gnss_GpsTimeProto_descriptor = (Descriptor)getDescriptor().getMessageTypes().get(0);
        internal_static_gnss_GpsTimeProto_fieldAccessorTable = new FieldAccessorTable(internal_static_gnss_GpsTimeProto_descriptor, new String[]{"Nanosecond"});
        internal_static_gnss_IonosphericModelProto_descriptor = (Descriptor)getDescriptor().getMessageTypes().get(1);
        internal_static_gnss_IonosphericModelProto_fieldAccessorTable = new FieldAccessorTable(internal_static_gnss_IonosphericModelProto_descriptor, new String[]{"DataTime", "Alpha", "Beta"});
    }

    public static final class IonosphericModelProto extends GeneratedMessageV3 implements Ephemeris.IonosphericModelProtoOrBuilder {
        private static final long serialVersionUID = 0L;
        private int bitField0_;
        public static final int DATA_TIME_FIELD_NUMBER = 1;
        private Ephemeris.GpsTimeProto dataTime_;
        public static final int ALPHA_FIELD_NUMBER = 2;
        private List<Double> alpha_;
        public static final int BETA_FIELD_NUMBER = 3;
        private List<Double> beta_;
        private byte memoizedIsInitialized;
        private static final Ephemeris.IonosphericModelProto DEFAULT_INSTANCE = new Ephemeris.IonosphericModelProto();
        /** @deprecated */
        @Deprecated
        public static final Parser<Ephemeris.IonosphericModelProto> PARSER = new AbstractParser<Ephemeris.IonosphericModelProto>() {
            public Ephemeris.IonosphericModelProto parsePartialFrom(CodedInputStream input, ExtensionRegistryLite extensionRegistry) throws InvalidProtocolBufferException {
                return new Ephemeris.IonosphericModelProto(input, extensionRegistry);
            }
        };

        private IonosphericModelProto(com.google.protobuf.GeneratedMessageV3.Builder<?> builder) {
            super(builder);
            this.memoizedIsInitialized = -1;
        }

        private IonosphericModelProto() {
            this.memoizedIsInitialized = -1;
            this.alpha_ = Collections.emptyList();
            this.beta_ = Collections.emptyList();
        }

        public final UnknownFieldSet getUnknownFields() {
            return this.unknownFields;
        }

        private IonosphericModelProto(CodedInputStream input, ExtensionRegistryLite extensionRegistry) throws InvalidProtocolBufferException {
            this();
            if (extensionRegistry == null) {
                throw new NullPointerException();
            } else {
                int mutable_bitField0_ = 0;
                com.google.protobuf.UnknownFieldSet.Builder unknownFields = UnknownFieldSet.newBuilder();

                try {
                    boolean done = false;

                    while(true) {
                        while(!done) {
                            int tag = input.readTag();
                            int length;
                            int limit;
                            switch(tag) {
                                case 0:
                                    done = true;
                                    continue;
                                case 10:
                                    Ephemeris.GpsTimeProto.Builder subBuilder = null;
                                    if ((this.bitField0_ & 1) == 1) {
                                        subBuilder = this.dataTime_.toBuilder();
                                    }

                                    this.dataTime_ = (Ephemeris.GpsTimeProto)input.readMessage(Ephemeris.GpsTimeProto.PARSER, extensionRegistry);
                                    if (subBuilder != null) {
                                        subBuilder.mergeFrom(this.dataTime_);
                                        this.dataTime_ = subBuilder.buildPartial();
                                    }

                                    this.bitField0_ |= 1;
                                    continue;
                                case 17:
                                    if ((mutable_bitField0_ & 2) != 2) {
                                        this.alpha_ = new ArrayList();
                                        mutable_bitField0_ |= 2;
                                    }

                                    this.alpha_.add(input.readDouble());
                                    continue;
                                case 18:
                                    length = input.readRawVarint32();
                                    limit = input.pushLimit(length);
                                    if ((mutable_bitField0_ & 2) != 2 && input.getBytesUntilLimit() > 0) {
                                        this.alpha_ = new ArrayList();
                                        mutable_bitField0_ |= 2;
                                    }
                                    break;
                                case 25:
                                    if ((mutable_bitField0_ & 4) != 4) {
                                        this.beta_ = new ArrayList();
                                        mutable_bitField0_ |= 4;
                                    }

                                    this.beta_.add(input.readDouble());
                                    continue;
                                case 26:
                                    length = input.readRawVarint32();
                                    limit = input.pushLimit(length);
                                    if ((mutable_bitField0_ & 4) != 4 && input.getBytesUntilLimit() > 0) {
                                        this.beta_ = new ArrayList();
                                        mutable_bitField0_ |= 4;
                                    }

                                    while(input.getBytesUntilLimit() > 0) {
                                        this.beta_.add(input.readDouble());
                                    }

                                    input.popLimit(limit);
                                    continue;
                                default:
                                    if (!this.parseUnknownField(input, unknownFields, extensionRegistry, tag)) {
                                        done = true;
                                    }
                                    continue;
                            }

                            while(input.getBytesUntilLimit() > 0) {
                                this.alpha_.add(input.readDouble());
                            }

                            input.popLimit(limit);
                        }

                        return;
                    }
                } catch (InvalidProtocolBufferException var13) {
                    throw var13.setUnfinishedMessage(this);
                } catch (IOException var14) {
                    throw (new InvalidProtocolBufferException(var14)).setUnfinishedMessage(this);
                } finally {
                    if ((mutable_bitField0_ & 2) == 2) {
                        this.alpha_ = Collections.unmodifiableList(this.alpha_);
                    }

                    if ((mutable_bitField0_ & 4) == 4) {
                        this.beta_ = Collections.unmodifiableList(this.beta_);
                    }

                    this.unknownFields = unknownFields.build();
                    this.makeExtensionsImmutable();
                }
            }
        }

        public static final Descriptor getDescriptor() {
            return Ephemeris.internal_static_gnss_IonosphericModelProto_descriptor;
        }

        protected FieldAccessorTable internalGetFieldAccessorTable() {
            return Ephemeris.internal_static_gnss_IonosphericModelProto_fieldAccessorTable.ensureFieldAccessorsInitialized(Ephemeris.IonosphericModelProto.class, Ephemeris.IonosphericModelProto.Builder.class);
        }

        public boolean hasDataTime() {
            return (this.bitField0_ & 1) == 1;
        }

        public Ephemeris.GpsTimeProto getDataTime() {
            return this.dataTime_ == null ? Ephemeris.GpsTimeProto.getDefaultInstance() : this.dataTime_;
        }

        public Ephemeris.GpsTimeProtoOrBuilder getDataTimeOrBuilder() {
            return this.dataTime_ == null ? Ephemeris.GpsTimeProto.getDefaultInstance() : this.dataTime_;
        }

        public List<Double> getAlphaList() {
            return this.alpha_;
        }

        public int getAlphaCount() {
            return this.alpha_.size();
        }

        public double getAlpha(int index) {
            return (Double)this.alpha_.get(index);
        }

        public List<Double> getBetaList() {
            return this.beta_;
        }

        public int getBetaCount() {
            return this.beta_.size();
        }

        public double getBeta(int index) {
            return (Double)this.beta_.get(index);
        }

        public final boolean isInitialized() {
            byte isInitialized = this.memoizedIsInitialized;
            if (isInitialized == 1) {
                return true;
            } else if (isInitialized == 0) {
                return false;
            } else {
                this.memoizedIsInitialized = 1;
                return true;
            }
        }

        public void writeTo(CodedOutputStream output) throws IOException {
            if ((this.bitField0_ & 1) == 1) {
                output.writeMessage(1, this.getDataTime());
            }

            int i;
            for(i = 0; i < this.alpha_.size(); ++i) {
                output.writeDouble(2, (Double)this.alpha_.get(i));
            }

            for(i = 0; i < this.beta_.size(); ++i) {
                output.writeDouble(3, (Double)this.beta_.get(i));
            }

            this.unknownFields.writeTo(output);
        }

        public int getSerializedSize() {
            int size = this.memoizedSize;
            if (size != -1) {
                return size;
            } else {
                size = 0;
                if ((this.bitField0_ & 1) == 1) {
                    size += CodedOutputStream.computeMessageSize(1, this.getDataTime());
                }

                int dataSize = 8 * this.getAlphaList().size();
                size += dataSize;
                size += 1 * this.getAlphaList().size();
                dataSize = 8 * this.getBetaList().size();
                size += dataSize;
                size += 1 * this.getBetaList().size();
                size += this.unknownFields.getSerializedSize();
                this.memoizedSize = size;
                return size;
            }
        }

        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            } else if (!(obj instanceof Ephemeris.IonosphericModelProto)) {
                return super.equals(obj);
            } else {
                Ephemeris.IonosphericModelProto other = (Ephemeris.IonosphericModelProto)obj;
                boolean result = true;
                result = result && this.hasDataTime() == other.hasDataTime();
                if (this.hasDataTime()) {
                    result = result && this.getDataTime().equals(other.getDataTime());
                }

                result = result && this.getAlphaList().equals(other.getAlphaList());
                result = result && this.getBetaList().equals(other.getBetaList());
                result = result && this.unknownFields.equals(other.unknownFields);
                return result;
            }
        }

        public int hashCode() {
            if (this.memoizedHashCode != 0) {
                return this.memoizedHashCode;
            } else {
                int a = 0;
                int hash = 19 * a + getDescriptor().hashCode();
                if (this.hasDataTime()) {
                    hash = 37 * hash + 1;
                    hash = 53 * hash + this.getDataTime().hashCode();
                }

                if (this.getAlphaCount() > 0) {
                    hash = 37 * hash + 2;
                    hash = 53 * hash + this.getAlphaList().hashCode();
                }

                if (this.getBetaCount() > 0) {
                    hash = 37 * hash + 3;
                    hash = 53 * hash + this.getBetaList().hashCode();
                }

                hash = 29 * hash + this.unknownFields.hashCode();
                this.memoizedHashCode = hash;
                return hash;
            }
        }

        public static Ephemeris.IonosphericModelProto parseFrom(ByteBuffer data) throws InvalidProtocolBufferException {
            return (Ephemeris.IonosphericModelProto)PARSER.parseFrom(data);
        }

        public static Ephemeris.IonosphericModelProto parseFrom(ByteBuffer data, ExtensionRegistryLite extensionRegistry) throws InvalidProtocolBufferException {
            return (Ephemeris.IonosphericModelProto)PARSER.parseFrom(data, extensionRegistry);
        }

        public static Ephemeris.IonosphericModelProto parseFrom(ByteString data) throws InvalidProtocolBufferException {
            return (Ephemeris.IonosphericModelProto)PARSER.parseFrom(data);
        }

        public static Ephemeris.IonosphericModelProto parseFrom(ByteString data, ExtensionRegistryLite extensionRegistry) throws InvalidProtocolBufferException {
            return (Ephemeris.IonosphericModelProto)PARSER.parseFrom(data, extensionRegistry);
        }

        public static Ephemeris.IonosphericModelProto parseFrom(byte[] data) throws InvalidProtocolBufferException {
            return (Ephemeris.IonosphericModelProto)PARSER.parseFrom(data);
        }

        public static Ephemeris.IonosphericModelProto parseFrom(byte[] data, ExtensionRegistryLite extensionRegistry) throws InvalidProtocolBufferException {
            return (Ephemeris.IonosphericModelProto)PARSER.parseFrom(data, extensionRegistry);
        }

        public static Ephemeris.IonosphericModelProto parseFrom(InputStream input) throws IOException {
            return (Ephemeris.IonosphericModelProto)GeneratedMessageV3.parseWithIOException(PARSER, input);
        }

        public static Ephemeris.IonosphericModelProto parseFrom(InputStream input, ExtensionRegistryLite extensionRegistry) throws IOException {
            return (Ephemeris.IonosphericModelProto)GeneratedMessageV3.parseWithIOException(PARSER, input, extensionRegistry);
        }

        public static Ephemeris.IonosphericModelProto parseDelimitedFrom(InputStream input) throws IOException {
            return (Ephemeris.IonosphericModelProto)GeneratedMessageV3.parseDelimitedWithIOException(PARSER, input);
        }

        public static Ephemeris.IonosphericModelProto parseDelimitedFrom(InputStream input, ExtensionRegistryLite extensionRegistry) throws IOException {
            return (Ephemeris.IonosphericModelProto)GeneratedMessageV3.parseDelimitedWithIOException(PARSER, input, extensionRegistry);
        }

        public static Ephemeris.IonosphericModelProto parseFrom(CodedInputStream input) throws IOException {
            return (Ephemeris.IonosphericModelProto)GeneratedMessageV3.parseWithIOException(PARSER, input);
        }

        public static Ephemeris.IonosphericModelProto parseFrom(CodedInputStream input, ExtensionRegistryLite extensionRegistry) throws IOException {
            return (Ephemeris.IonosphericModelProto)GeneratedMessageV3.parseWithIOException(PARSER, input, extensionRegistry);
        }

        public Ephemeris.IonosphericModelProto.Builder newBuilderForType() {
            return newBuilder();
        }

        public static Ephemeris.IonosphericModelProto.Builder newBuilder() {
            return DEFAULT_INSTANCE.toBuilder();
        }

        public static Ephemeris.IonosphericModelProto.Builder newBuilder(Ephemeris.IonosphericModelProto prototype) {
            return DEFAULT_INSTANCE.toBuilder().mergeFrom(prototype);
        }

        public Ephemeris.IonosphericModelProto.Builder toBuilder() {
            return this == DEFAULT_INSTANCE ? new Ephemeris.IonosphericModelProto.Builder() : (new Ephemeris.IonosphericModelProto.Builder()).mergeFrom(this);
        }

        protected Ephemeris.IonosphericModelProto.Builder newBuilderForType(BuilderParent parent) {
            Ephemeris.IonosphericModelProto.Builder builder = new Ephemeris.IonosphericModelProto.Builder(parent);
            return builder;
        }

        public static Ephemeris.IonosphericModelProto getDefaultInstance() {
            return DEFAULT_INSTANCE;
        }

        public static Parser<Ephemeris.IonosphericModelProto> parser() {
            return PARSER;
        }

        public Parser<Ephemeris.IonosphericModelProto> getParserForType() {
            return PARSER;
        }

        public Ephemeris.IonosphericModelProto getDefaultInstanceForType() {
            return DEFAULT_INSTANCE;
        }

        public static final class Builder extends com.google.protobuf.GeneratedMessageV3.Builder<Ephemeris.IonosphericModelProto.Builder> implements Ephemeris.IonosphericModelProtoOrBuilder {
            private int bitField0_;
            private Ephemeris.GpsTimeProto dataTime_;
            private SingleFieldBuilderV3<Ephemeris.GpsTimeProto, Ephemeris.GpsTimeProto.Builder, Ephemeris.GpsTimeProtoOrBuilder> dataTimeBuilder_;
            private List<Double> alpha_;
            private List<Double> beta_;

            public static final Descriptor getDescriptor() {
                return Ephemeris.internal_static_gnss_IonosphericModelProto_descriptor;
            }

            protected FieldAccessorTable internalGetFieldAccessorTable() {
                return Ephemeris.internal_static_gnss_IonosphericModelProto_fieldAccessorTable.ensureFieldAccessorsInitialized(Ephemeris.IonosphericModelProto.class, Ephemeris.IonosphericModelProto.Builder.class);
            }

            private Builder() {
                this.dataTime_ = null;
                this.alpha_ = Collections.emptyList();
                this.beta_ = Collections.emptyList();
                this.maybeForceBuilderInitialization();
            }

            private Builder(BuilderParent parent) {
                super(parent);
                this.dataTime_ = null;
                this.alpha_ = Collections.emptyList();
                this.beta_ = Collections.emptyList();
                this.maybeForceBuilderInitialization();
            }

            private void maybeForceBuilderInitialization() {
                if (Ephemeris.IonosphericModelProto.alwaysUseFieldBuilders) {
                    this.getDataTimeFieldBuilder();
                }

            }

            public Ephemeris.IonosphericModelProto.Builder clear() {
                super.clear();
                if (this.dataTimeBuilder_ == null) {
                    this.dataTime_ = null;
                } else {
                    this.dataTimeBuilder_.clear();
                }

                this.bitField0_ &= -2;
                this.alpha_ = Collections.emptyList();
                this.bitField0_ &= -3;
                this.beta_ = Collections.emptyList();
                this.bitField0_ &= -5;
                return this;
            }

            public Descriptor getDescriptorForType() {
                return Ephemeris.internal_static_gnss_IonosphericModelProto_descriptor;
            }

            public Ephemeris.IonosphericModelProto getDefaultInstanceForType() {
                return Ephemeris.IonosphericModelProto.getDefaultInstance();
            }

            public Ephemeris.IonosphericModelProto build() {
                Ephemeris.IonosphericModelProto result = this.buildPartial();
                if (!result.isInitialized()) {
                    throw newUninitializedMessageException(result);
                } else {
                    return result;
                }
            }

            public Ephemeris.IonosphericModelProto buildPartial() {
                Ephemeris.IonosphericModelProto result = new Ephemeris.IonosphericModelProto(this);
                int from_bitField0_ = this.bitField0_;
                int to_bitField0_ = 0;
                if ((from_bitField0_ & 1) == 1) {
                    to_bitField0_ |= 1;
                }

                if (this.dataTimeBuilder_ == null) {
                    result.dataTime_ = this.dataTime_;
                } else {
                    result.dataTime_ = (Ephemeris.GpsTimeProto)this.dataTimeBuilder_.build();
                }

                if ((this.bitField0_ & 2) == 2) {
                    this.alpha_ = Collections.unmodifiableList(this.alpha_);
                    this.bitField0_ &= -3;
                }

                result.alpha_ = this.alpha_;
                if ((this.bitField0_ & 4) == 4) {
                    this.beta_ = Collections.unmodifiableList(this.beta_);
                    this.bitField0_ &= -5;
                }

                result.beta_ = this.beta_;
                result.bitField0_ = to_bitField0_;
                this.onBuilt();
                return result;
            }

            public Ephemeris.IonosphericModelProto.Builder clone() {
                return (Ephemeris.IonosphericModelProto.Builder)super.clone();
            }

            public Ephemeris.IonosphericModelProto.Builder setField(FieldDescriptor field, Object value) {
                return (Ephemeris.IonosphericModelProto.Builder)super.setField(field, value);
            }

            public Ephemeris.IonosphericModelProto.Builder clearField(FieldDescriptor field) {
                return (Ephemeris.IonosphericModelProto.Builder)super.clearField(field);
            }

            public Ephemeris.IonosphericModelProto.Builder clearOneof(OneofDescriptor oneof) {
                return (Ephemeris.IonosphericModelProto.Builder)super.clearOneof(oneof);
            }

            public Ephemeris.IonosphericModelProto.Builder setRepeatedField(FieldDescriptor field, int index, Object value) {
                return (Ephemeris.IonosphericModelProto.Builder)super.setRepeatedField(field, index, value);
            }

            public Ephemeris.IonosphericModelProto.Builder addRepeatedField(FieldDescriptor field, Object value) {
                return (Ephemeris.IonosphericModelProto.Builder)super.addRepeatedField(field, value);
            }

            public Ephemeris.IonosphericModelProto.Builder mergeFrom(Message other) {
                if (other instanceof Ephemeris.IonosphericModelProto) {
                    return this.mergeFrom((Ephemeris.IonosphericModelProto)other);
                } else {
                    super.mergeFrom(other);
                    return this;
                }
            }

            public Ephemeris.IonosphericModelProto.Builder mergeFrom(Ephemeris.IonosphericModelProto other) {
                if (other == Ephemeris.IonosphericModelProto.getDefaultInstance()) {
                    return this;
                } else {
                    if (other.hasDataTime()) {
                        this.mergeDataTime(other.getDataTime());
                    }

                    if (!other.alpha_.isEmpty()) {
                        if (this.alpha_.isEmpty()) {
                            this.alpha_ = other.alpha_;
                            this.bitField0_ &= -3;
                        } else {
                            this.ensureAlphaIsMutable();
                            this.alpha_.addAll(other.alpha_);
                        }

                        this.onChanged();
                    }

                    if (!other.beta_.isEmpty()) {
                        if (this.beta_.isEmpty()) {
                            this.beta_ = other.beta_;
                            this.bitField0_ &= -5;
                        } else {
                            this.ensureBetaIsMutable();
                            this.beta_.addAll(other.beta_);
                        }

                        this.onChanged();
                    }

                    this.mergeUnknownFields(other.unknownFields);
                    this.onChanged();
                    return this;
                }
            }

            public final boolean isInitialized() {
                return true;
            }

            public Ephemeris.IonosphericModelProto.Builder mergeFrom(CodedInputStream input, ExtensionRegistryLite extensionRegistry) throws IOException {
                Ephemeris.IonosphericModelProto parsedMessage = null;

                try {
                    parsedMessage = (Ephemeris.IonosphericModelProto)Ephemeris.IonosphericModelProto.PARSER.parsePartialFrom(input, extensionRegistry);
                } catch (InvalidProtocolBufferException var8) {
                    parsedMessage = (Ephemeris.IonosphericModelProto)var8.getUnfinishedMessage();
                    throw var8.unwrapIOException();
                } finally {
                    if (parsedMessage != null) {
                        this.mergeFrom(parsedMessage);
                    }

                }

                return this;
            }

            public boolean hasDataTime() {
                return (this.bitField0_ & 1) == 1;
            }

            public Ephemeris.GpsTimeProto getDataTime() {
                if (this.dataTimeBuilder_ == null) {
                    return this.dataTime_ == null ? Ephemeris.GpsTimeProto.getDefaultInstance() : this.dataTime_;
                } else {
                    return (Ephemeris.GpsTimeProto)this.dataTimeBuilder_.getMessage();
                }
            }

            public Ephemeris.IonosphericModelProto.Builder setDataTime(Ephemeris.GpsTimeProto value) {
                if (this.dataTimeBuilder_ == null) {
                    if (value == null) {
                        throw new NullPointerException();
                    }

                    this.dataTime_ = value;
                    this.onChanged();
                } else {
                    this.dataTimeBuilder_.setMessage(value);
                }

                this.bitField0_ |= 1;
                return this;
            }

            public Ephemeris.IonosphericModelProto.Builder setDataTime(Ephemeris.GpsTimeProto.Builder builderForValue) {
                if (this.dataTimeBuilder_ == null) {
                    this.dataTime_ = builderForValue.build();
                    this.onChanged();
                } else {
                    this.dataTimeBuilder_.setMessage(builderForValue.build());
                }

                this.bitField0_ |= 1;
                return this;
            }

            public Ephemeris.IonosphericModelProto.Builder mergeDataTime(Ephemeris.GpsTimeProto value) {
                if (this.dataTimeBuilder_ == null) {
                    if ((this.bitField0_ & 1) == 1 && this.dataTime_ != null && this.dataTime_ != Ephemeris.GpsTimeProto.getDefaultInstance()) {
                        this.dataTime_ = Ephemeris.GpsTimeProto.newBuilder(this.dataTime_).mergeFrom(value).buildPartial();
                    } else {
                        this.dataTime_ = value;
                    }

                    this.onChanged();
                } else {
                    this.dataTimeBuilder_.mergeFrom(value);
                }

                this.bitField0_ |= 1;
                return this;
            }

            public Ephemeris.IonosphericModelProto.Builder clearDataTime() {
                if (this.dataTimeBuilder_ == null) {
                    this.dataTime_ = null;
                    this.onChanged();
                } else {
                    this.dataTimeBuilder_.clear();
                }

                this.bitField0_ &= -2;
                return this;
            }

            public Ephemeris.GpsTimeProto.Builder getDataTimeBuilder() {
                this.bitField0_ |= 1;
                this.onChanged();
                return (Ephemeris.GpsTimeProto.Builder)this.getDataTimeFieldBuilder().getBuilder();
            }

            public Ephemeris.GpsTimeProtoOrBuilder getDataTimeOrBuilder() {
                if (this.dataTimeBuilder_ != null) {
                    return (Ephemeris.GpsTimeProtoOrBuilder)this.dataTimeBuilder_.getMessageOrBuilder();
                } else {
                    return this.dataTime_ == null ? Ephemeris.GpsTimeProto.getDefaultInstance() : this.dataTime_;
                }
            }

            private SingleFieldBuilderV3<Ephemeris.GpsTimeProto, Ephemeris.GpsTimeProto.Builder, Ephemeris.GpsTimeProtoOrBuilder> getDataTimeFieldBuilder() {
                if (this.dataTimeBuilder_ == null) {
                    this.dataTimeBuilder_ = new SingleFieldBuilderV3(this.getDataTime(), this.getParentForChildren(), this.isClean());
                    this.dataTime_ = null;
                }

                return this.dataTimeBuilder_;
            }

            private void ensureAlphaIsMutable() {
                if ((this.bitField0_ & 2) != 2) {
                    this.alpha_ = new ArrayList(this.alpha_);
                    this.bitField0_ |= 2;
                }

            }

            public List<Double> getAlphaList() {
                return Collections.unmodifiableList(this.alpha_);
            }

            public int getAlphaCount() {
                return this.alpha_.size();
            }

            public double getAlpha(int index) {
                return (Double)this.alpha_.get(index);
            }

            public Ephemeris.IonosphericModelProto.Builder setAlpha(int index, double value) {
                this.ensureAlphaIsMutable();
                this.alpha_.set(index, value);
                this.onChanged();
                return this;
            }

            public Ephemeris.IonosphericModelProto.Builder addAlpha(double value) {
                this.ensureAlphaIsMutable();
                this.alpha_.add(value);
                this.onChanged();
                return this;
            }

            public Ephemeris.IonosphericModelProto.Builder addAllAlpha(Iterable<? extends Double> values) {
                this.ensureAlphaIsMutable();
                com.google.protobuf.AbstractMessageLite.Builder.addAll(values, this.alpha_);
                this.onChanged();
                return this;
            }

            public Ephemeris.IonosphericModelProto.Builder clearAlpha() {
                this.alpha_ = Collections.emptyList();
                this.bitField0_ &= -3;
                this.onChanged();
                return this;
            }

            private void ensureBetaIsMutable() {
                if ((this.bitField0_ & 4) != 4) {
                    this.beta_ = new ArrayList(this.beta_);
                    this.bitField0_ |= 4;
                }

            }

            public List<Double> getBetaList() {
                return Collections.unmodifiableList(this.beta_);
            }

            public int getBetaCount() {
                return this.beta_.size();
            }

            public double getBeta(int index) {
                return (Double)this.beta_.get(index);
            }

            public Ephemeris.IonosphericModelProto.Builder setBeta(int index, double value) {
                this.ensureBetaIsMutable();
                this.beta_.set(index, value);
                this.onChanged();
                return this;
            }

            public Ephemeris.IonosphericModelProto.Builder addBeta(double value) {
                this.ensureBetaIsMutable();
                this.beta_.add(value);
                this.onChanged();
                return this;
            }

            public Ephemeris.IonosphericModelProto.Builder addAllBeta(Iterable<? extends Double> values) {
                this.ensureBetaIsMutable();
                com.google.protobuf.AbstractMessageLite.Builder.addAll(values, this.beta_);
                this.onChanged();
                return this;
            }

            public Ephemeris.IonosphericModelProto.Builder clearBeta() {
                this.beta_ = Collections.emptyList();
                this.bitField0_ &= -5;
                this.onChanged();
                return this;
            }

            public final Ephemeris.IonosphericModelProto.Builder setUnknownFields(UnknownFieldSet unknownFields) {
                return (Ephemeris.IonosphericModelProto.Builder)super.setUnknownFields(unknownFields);
            }

            public final Ephemeris.IonosphericModelProto.Builder mergeUnknownFields(UnknownFieldSet unknownFields) {
                return (Ephemeris.IonosphericModelProto.Builder)super.mergeUnknownFields(unknownFields);
            }
        }
    }

    public interface IonosphericModelProtoOrBuilder extends MessageOrBuilder {
        boolean hasDataTime();

        Ephemeris.GpsTimeProto getDataTime();

        Ephemeris.GpsTimeProtoOrBuilder getDataTimeOrBuilder();

        List<Double> getAlphaList();

        int getAlphaCount();

        double getAlpha(int var1);

        List<Double> getBetaList();

        int getBetaCount();

        double getBeta(int var1);
    }

    public static final class GpsTimeProto extends GeneratedMessageV3 implements Ephemeris.GpsTimeProtoOrBuilder {
        private static final long serialVersionUID = 0L;
        private int bitField0_;
        public static final int NANOSECOND_FIELD_NUMBER = 1;
        private long nanosecond_;
        private byte memoizedIsInitialized;
        private static final Ephemeris.GpsTimeProto DEFAULT_INSTANCE = new Ephemeris.GpsTimeProto();
        /** @deprecated */
        @Deprecated
        public static final Parser<Ephemeris.GpsTimeProto> PARSER = new AbstractParser<Ephemeris.GpsTimeProto>() {
            public Ephemeris.GpsTimeProto parsePartialFrom(CodedInputStream input, ExtensionRegistryLite extensionRegistry) throws InvalidProtocolBufferException {
                return new Ephemeris.GpsTimeProto(input, extensionRegistry);
            }
        };

        private GpsTimeProto(com.google.protobuf.GeneratedMessageV3.Builder<?> builder) {
            super(builder);
            this.memoizedIsInitialized = -1;
        }

        private GpsTimeProto() {
            this.memoizedIsInitialized = -1;
            this.nanosecond_ = 0L;
        }

        public final UnknownFieldSet getUnknownFields() {
            return this.unknownFields;
        }

        private GpsTimeProto(CodedInputStream input, ExtensionRegistryLite extensionRegistry) throws InvalidProtocolBufferException {
            this();
            if (extensionRegistry == null) {
                throw new NullPointerException();
            } else {
                com.google.protobuf.UnknownFieldSet.Builder unknownFields = UnknownFieldSet.newBuilder();

                try {
                    boolean done = false;

                    while(!done) {
                        int tag = input.readTag();
                        switch(tag) {
                            case 0:
                                done = true;
                                break;
                            case 8:
                                this.bitField0_ |= 1;
                                this.nanosecond_ = input.readInt64();
                                break;
                            default:
                                if (!this.parseUnknownField(input, unknownFields, extensionRegistry, tag)) {
                                    done = true;
                                }
                        }
                    }
                } catch (InvalidProtocolBufferException var11) {
                    throw var11.setUnfinishedMessage(this);
                } catch (IOException var12) {
                    throw (new InvalidProtocolBufferException(var12)).setUnfinishedMessage(this);
                } finally {
                    this.unknownFields = unknownFields.build();
                    this.makeExtensionsImmutable();
                }

            }
        }

        public static final Descriptor getDescriptor() {
            return Ephemeris.internal_static_gnss_GpsTimeProto_descriptor;
        }

        protected FieldAccessorTable internalGetFieldAccessorTable() {
            return Ephemeris.internal_static_gnss_GpsTimeProto_fieldAccessorTable.ensureFieldAccessorsInitialized(Ephemeris.GpsTimeProto.class, Ephemeris.GpsTimeProto.Builder.class);
        }

        public boolean hasNanosecond() {
            return (this.bitField0_ & 1) == 1;
        }

        public long getNanosecond() {
            return this.nanosecond_;
        }

        public final boolean isInitialized() {
            byte isInitialized = this.memoizedIsInitialized;
            if (isInitialized == 1) {
                return true;
            } else if (isInitialized == 0) {
                return false;
            } else {
                this.memoizedIsInitialized = 1;
                return true;
            }
        }

        public void writeTo(CodedOutputStream output) throws IOException {
            if ((this.bitField0_ & 1) == 1) {
                output.writeInt64(1, this.nanosecond_);
            }

            this.unknownFields.writeTo(output);
        }

        public int getSerializedSize() {
            int size = this.memoizedSize;
            if (size != -1) {
                return size;
            } else {
                size = 0;
                if ((this.bitField0_ & 1) == 1) {
                    size += CodedOutputStream.computeInt64Size(1, this.nanosecond_);
                }

                size += this.unknownFields.getSerializedSize();
                this.memoizedSize = size;
                return size;
            }
        }

        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            } else if (!(obj instanceof Ephemeris.GpsTimeProto)) {
                return super.equals(obj);
            } else {
                Ephemeris.GpsTimeProto other = (Ephemeris.GpsTimeProto)obj;
                boolean result = true;
                result = result && this.hasNanosecond() == other.hasNanosecond();
                if (this.hasNanosecond()) {
                    result = result && this.getNanosecond() == other.getNanosecond();
                }

                result = result && this.unknownFields.equals(other.unknownFields);
                return result;
            }
        }

        public int hashCode() {
            if (this.memoizedHashCode != 0) {
                return this.memoizedHashCode;
            } else {
                int a = 0;
                int hash = 19 * a + getDescriptor().hashCode();
                if (this.hasNanosecond()) {
                    hash = 37 * hash + 1;
                    hash = 53 * hash + Internal.hashLong(this.getNanosecond());
                }

                hash = 29 * hash + this.unknownFields.hashCode();
                this.memoizedHashCode = hash;
                return hash;
            }
        }

        public static Ephemeris.GpsTimeProto parseFrom(ByteBuffer data) throws InvalidProtocolBufferException {
            return (Ephemeris.GpsTimeProto)PARSER.parseFrom(data);
        }

        public static Ephemeris.GpsTimeProto parseFrom(ByteBuffer data, ExtensionRegistryLite extensionRegistry) throws InvalidProtocolBufferException {
            return (Ephemeris.GpsTimeProto)PARSER.parseFrom(data, extensionRegistry);
        }

        public static Ephemeris.GpsTimeProto parseFrom(ByteString data) throws InvalidProtocolBufferException {
            return (Ephemeris.GpsTimeProto)PARSER.parseFrom(data);
        }

        public static Ephemeris.GpsTimeProto parseFrom(ByteString data, ExtensionRegistryLite extensionRegistry) throws InvalidProtocolBufferException {
            return (Ephemeris.GpsTimeProto)PARSER.parseFrom(data, extensionRegistry);
        }

        public static Ephemeris.GpsTimeProto parseFrom(byte[] data) throws InvalidProtocolBufferException {
            return (Ephemeris.GpsTimeProto)PARSER.parseFrom(data);
        }

        public static Ephemeris.GpsTimeProto parseFrom(byte[] data, ExtensionRegistryLite extensionRegistry) throws InvalidProtocolBufferException {
            return (Ephemeris.GpsTimeProto)PARSER.parseFrom(data, extensionRegistry);
        }

        public static Ephemeris.GpsTimeProto parseFrom(InputStream input) throws IOException {
            return (Ephemeris.GpsTimeProto)GeneratedMessageV3.parseWithIOException(PARSER, input);
        }

        public static Ephemeris.GpsTimeProto parseFrom(InputStream input, ExtensionRegistryLite extensionRegistry) throws IOException {
            return (Ephemeris.GpsTimeProto)GeneratedMessageV3.parseWithIOException(PARSER, input, extensionRegistry);
        }

        public static Ephemeris.GpsTimeProto parseDelimitedFrom(InputStream input) throws IOException {
            return (Ephemeris.GpsTimeProto)GeneratedMessageV3.parseDelimitedWithIOException(PARSER, input);
        }

        public static Ephemeris.GpsTimeProto parseDelimitedFrom(InputStream input, ExtensionRegistryLite extensionRegistry) throws IOException {
            return (Ephemeris.GpsTimeProto)GeneratedMessageV3.parseDelimitedWithIOException(PARSER, input, extensionRegistry);
        }

        public static Ephemeris.GpsTimeProto parseFrom(CodedInputStream input) throws IOException {
            return (Ephemeris.GpsTimeProto)GeneratedMessageV3.parseWithIOException(PARSER, input);
        }

        public static Ephemeris.GpsTimeProto parseFrom(CodedInputStream input, ExtensionRegistryLite extensionRegistry) throws IOException {
            return (Ephemeris.GpsTimeProto)GeneratedMessageV3.parseWithIOException(PARSER, input, extensionRegistry);
        }

        public Ephemeris.GpsTimeProto.Builder newBuilderForType() {
            return newBuilder();
        }

        public static Ephemeris.GpsTimeProto.Builder newBuilder() {
            return DEFAULT_INSTANCE.toBuilder();
        }

        public static Ephemeris.GpsTimeProto.Builder newBuilder(Ephemeris.GpsTimeProto prototype) {
            return DEFAULT_INSTANCE.toBuilder().mergeFrom(prototype);
        }

        public Ephemeris.GpsTimeProto.Builder toBuilder() {
            return this == DEFAULT_INSTANCE ? new Ephemeris.GpsTimeProto.Builder() : (new Ephemeris.GpsTimeProto.Builder()).mergeFrom(this);
        }

        protected Ephemeris.GpsTimeProto.Builder newBuilderForType(BuilderParent parent) {
            Ephemeris.GpsTimeProto.Builder builder = new Ephemeris.GpsTimeProto.Builder(parent);
            return builder;
        }

        public static Ephemeris.GpsTimeProto getDefaultInstance() {
            return DEFAULT_INSTANCE;
        }

        public static Parser<Ephemeris.GpsTimeProto> parser() {
            return PARSER;
        }

        public Parser<Ephemeris.GpsTimeProto> getParserForType() {
            return PARSER;
        }

        public Ephemeris.GpsTimeProto getDefaultInstanceForType() {
            return DEFAULT_INSTANCE;
        }

        public static final class Builder extends com.google.protobuf.GeneratedMessageV3.Builder<Ephemeris.GpsTimeProto.Builder> implements Ephemeris.GpsTimeProtoOrBuilder {
            private int bitField0_;
            private long nanosecond_;

            public static final Descriptor getDescriptor() {
                return Ephemeris.internal_static_gnss_GpsTimeProto_descriptor;
            }

            protected FieldAccessorTable internalGetFieldAccessorTable() {
                return Ephemeris.internal_static_gnss_GpsTimeProto_fieldAccessorTable.ensureFieldAccessorsInitialized(Ephemeris.GpsTimeProto.class, Ephemeris.GpsTimeProto.Builder.class);
            }

            private Builder() {
                this.maybeForceBuilderInitialization();
            }

            private Builder(BuilderParent parent) {
                super(parent);
                this.maybeForceBuilderInitialization();
            }

            private void maybeForceBuilderInitialization() {
                if (Ephemeris.GpsTimeProto.alwaysUseFieldBuilders) {
                }

            }

            public Ephemeris.GpsTimeProto.Builder clear() {
                super.clear();
                this.nanosecond_ = 0L;
                this.bitField0_ &= -2;
                return this;
            }

            public Descriptor getDescriptorForType() {
                return Ephemeris.internal_static_gnss_GpsTimeProto_descriptor;
            }

            public Ephemeris.GpsTimeProto getDefaultInstanceForType() {
                return Ephemeris.GpsTimeProto.getDefaultInstance();
            }

            public Ephemeris.GpsTimeProto build() {
                Ephemeris.GpsTimeProto result = this.buildPartial();
                if (!result.isInitialized()) {
                    throw newUninitializedMessageException(result);
                } else {
                    return result;
                }
            }

            public Ephemeris.GpsTimeProto buildPartial() {
                Ephemeris.GpsTimeProto result = new Ephemeris.GpsTimeProto(this);
                int from_bitField0_ = this.bitField0_;
                int to_bitField0_ = 0;
                if ((from_bitField0_ & 1) == 1) {
                    to_bitField0_ |= 1;
                }

                result.nanosecond_ = this.nanosecond_;
                result.bitField0_ = to_bitField0_;
                this.onBuilt();
                return result;
            }

            public Ephemeris.GpsTimeProto.Builder clone() {
                return (Ephemeris.GpsTimeProto.Builder)super.clone();
            }

            public Ephemeris.GpsTimeProto.Builder setField(FieldDescriptor field, Object value) {
                return (Ephemeris.GpsTimeProto.Builder)super.setField(field, value);
            }

            public Ephemeris.GpsTimeProto.Builder clearField(FieldDescriptor field) {
                return (Ephemeris.GpsTimeProto.Builder)super.clearField(field);
            }

            public Ephemeris.GpsTimeProto.Builder clearOneof(OneofDescriptor oneof) {
                return (Ephemeris.GpsTimeProto.Builder)super.clearOneof(oneof);
            }

            public Ephemeris.GpsTimeProto.Builder setRepeatedField(FieldDescriptor field, int index, Object value) {
                return (Ephemeris.GpsTimeProto.Builder)super.setRepeatedField(field, index, value);
            }

            public Ephemeris.GpsTimeProto.Builder addRepeatedField(FieldDescriptor field, Object value) {
                return (Ephemeris.GpsTimeProto.Builder)super.addRepeatedField(field, value);
            }

            public Ephemeris.GpsTimeProto.Builder mergeFrom(Message other) {
                if (other instanceof Ephemeris.GpsTimeProto) {
                    return this.mergeFrom((Ephemeris.GpsTimeProto)other);
                } else {
                    super.mergeFrom(other);
                    return this;
                }
            }

            public Ephemeris.GpsTimeProto.Builder mergeFrom(Ephemeris.GpsTimeProto other) {
                if (other == Ephemeris.GpsTimeProto.getDefaultInstance()) {
                    return this;
                } else {
                    if (other.hasNanosecond()) {
                        this.setNanosecond(other.getNanosecond());
                    }

                    this.mergeUnknownFields(other.unknownFields);
                    this.onChanged();
                    return this;
                }
            }

            public final boolean isInitialized() {
                return true;
            }

            public Ephemeris.GpsTimeProto.Builder mergeFrom(CodedInputStream input, ExtensionRegistryLite extensionRegistry) throws IOException {
                Ephemeris.GpsTimeProto parsedMessage = null;

                try {
                    parsedMessage = (Ephemeris.GpsTimeProto)Ephemeris.GpsTimeProto.PARSER.parsePartialFrom(input, extensionRegistry);
                } catch (InvalidProtocolBufferException var8) {
                    parsedMessage = (Ephemeris.GpsTimeProto)var8.getUnfinishedMessage();
                    throw var8.unwrapIOException();
                } finally {
                    if (parsedMessage != null) {
                        this.mergeFrom(parsedMessage);
                    }

                }

                return this;
            }

            public boolean hasNanosecond() {
                return (this.bitField0_ & 1) == 1;
            }

            public long getNanosecond() {
                return this.nanosecond_;
            }

            public Ephemeris.GpsTimeProto.Builder setNanosecond(long value) {
                this.bitField0_ |= 1;
                this.nanosecond_ = value;
                this.onChanged();
                return this;
            }

            public Ephemeris.GpsTimeProto.Builder clearNanosecond() {
                this.bitField0_ &= -2;
                this.nanosecond_ = 0L;
                this.onChanged();
                return this;
            }

            public final Ephemeris.GpsTimeProto.Builder setUnknownFields(UnknownFieldSet unknownFields) {
                return (Ephemeris.GpsTimeProto.Builder)super.setUnknownFields(unknownFields);
            }

            public final Ephemeris.GpsTimeProto.Builder mergeUnknownFields(UnknownFieldSet unknownFields) {
                return (Ephemeris.GpsTimeProto.Builder)super.mergeUnknownFields(unknownFields);
            }
        }
    }

    public interface GpsTimeProtoOrBuilder extends MessageOrBuilder {
        boolean hasNanosecond();

        long getNanosecond();
    }
}
