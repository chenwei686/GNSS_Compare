package com.galfins.gogpsextracts.Gps;

import com.galfins.gogpsextracts.KeplerianEphemeris;

public class GpsEphemeris extends KeplerianEphemeris {
    public final double accuracyM;
    public final double tgdS;
    public final boolean fitIntvFlag;
    public final boolean l2PDataFlag;
    public final int codeL2;
    public final int iodc;

    private GpsEphemeris(GpsEphemeris.Builder builder) {
        super(builder);
        this.accuracyM = builder.accuracyM;
        this.tgdS = builder.tgdS;
        this.fitIntvFlag = builder.fitIntvFlag;
        this.l2PDataFlag = builder.l2PDataFlag;
        this.codeL2 = builder.codeL2;
        this.iodc = builder.iodc;
    }

    public static GpsEphemeris.Builder newBuilder() {
        return new GpsEphemeris.Builder();
    }

    public static class Builder extends KeplerianEphemeris.Builder<GpsEphemeris.Builder> {
        private double accuracyM;
        private double tgdS;
        private boolean fitIntvFlag;
        private boolean l2PDataFlag;
        private int codeL2;
        private int iodc;

        private Builder() {
        }

        public GpsEphemeris.Builder getThis() {
            return this;
        }

        public GpsEphemeris.Builder setAccuracyM(double accuracyM) {
            this.accuracyM = accuracyM;
            return this.getThis();
        }

        public GpsEphemeris.Builder setTgdS(double tgdS) {
            this.tgdS = tgdS;
            return this.getThis();
        }

        public GpsEphemeris.Builder setFitIntvFlag(boolean fitIntvFlag) {
            this.fitIntvFlag = fitIntvFlag;
            return this.getThis();
        }

        public GpsEphemeris.Builder setL2PDataFlag(boolean l2PDataFlag) {
            this.l2PDataFlag = l2PDataFlag;
            return this.getThis();
        }

        public GpsEphemeris.Builder setCodeL2(int codeL2) {
            this.codeL2 = codeL2;
            return this.getThis();
        }

        public GpsEphemeris.Builder setIodc(int iodc) {
            this.iodc = iodc;
            return this.getThis();
        }

        public GpsEphemeris build() {
            return new GpsEphemeris(this);
        }
    }
}